(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("tromey" . "http://tromey.com/elpa/") t)
(add-to-list 'package-archives
             '("elpy" . "http://jorgenschaefer.github.io/packages/") t)

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(defvar my-packages
  '(
    ;; Clojure/Lisp related
    paredit
    clojure-mode
    clojure-mode-extra-font-locking
    cider
    rainbow-delimiters
    eval-sexp-fu
    cider-eval-sexp-fu
    ;; below is python related...right now the ones I need are installed by pip
    ;; In time look into moving some to this startup list
    ;; python-mode
    ;; rope
    ;; ropemode
    ;; pymacs
    ;; ropemacs
    elpy
    epc
    jedi
    ;; for ocaml below
    merlin 
    tuareg
    utop
    ;; misc tools
    magit
    darcula-theme
    yasnippet
    projectile
    ido-ubiquitous
    smex))
        

(dolist (p my-packages)
  (when(not(package-installed-p p))
    (package-install p)))

;; for custom files and functions of all types

(add-to-list 'load-path "~/.emacs.d/custom/")

;; ensure environment and shell variables and brought in correctly for integrated sessions
(load "shell-integration.el")

;; for editing lisps for configuring emacs, esp Emacs Lisp. since we might always need to configure 
;; we should always load this regardless of the language of the project that we are working
(load "elisp-editing.el")

;; Various tweaks that should be moved into their own sections eventually.
(load "misc.el")

;; below are items that 
;; the brave and true guy clumped together into settings files 					
;; I am adding them simply to get things working, later move into their own files
;; according to my preferences
(load "navigation.el")
(load "editing.el")
(load "ui.el")

;; language specific settings, launched from their own elisp config in /emacs.d/custom
(load "setup-clojure.el")
(load "setup-python.el") 
;; (load "setup-c.el")
;; (load "setup-cpp.el")
;; (load "setup-racket.el") ;; compare with dr racket IDE and pick later
;; (load "setup-common-lisp.el") ;; Maybe do this? Look into SBCL
(load "setup-ocaml.el")
