# README #


NOTE: 

I've stop using my own emacs config and started using spacemacs. At some point I'll replace this config with my .spacemacs file.



===========================================================================

Lucas Falkenstein's emacs config (.emacs.d/). As always a work in progress. A real mess right now since much of it
was copy-pasted from tutorials as I started teaching myself. I know a little more so I do intend to go back and clean some of it up. For now the comments are ... a little strange, coming from me and other snippets I copied. 
Right now I have a working ocaml and clojure setups. Python and C are to come. 
Any and all recommendations are welcome. 

=============================================================================

Note that it downloads most packages it needs at startup so you should have a internet connection the first time.

For OCaml please follow the steps in Real World Ocaml [here](https://github.com/realworldocaml/book/wiki/Installation-Instructions) and [a nice utility to streamline some of the process](https://github.com/OCamlPro/opam-user-setup). If you use the utility remember to mv the auto generated el file into the customs folder since the setup-ocaml.el expects it there. Also remove the auto generated stub at the end of init.el. Alternatively, remove the (require 'opam-user-setup "~/.emacs.d/custom/opam-user-setup.el") in setup-ocaml.el and accept the tool's defaults. A few more details can be found in the custom/setup-ocaml.el. 

Clojure should work mostly out of the box. Do remember to check your ~/.lein/profiles.clj and make sure it has a listing for your version of nrepl. If not you can add (or create the file then add) 


```
#!clojure
{:user {:plugins [[cider/cider-nrepl "0.10.0-SNAPSHOT"]]:dependencies [[org.clojure/clojure "1.7.0"]]}}

```


If you run into issues with the nrepl, it's probably because the cider-nrepl version specified above doesn't match cider's version, use cider-version to check and change ~/.lein/profiles.clj to match.