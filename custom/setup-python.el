;; Python programming settings...
;; Uses elpy,(not right now but eventually rope)
;; jedi, flake8, importmagic, ipython and autopep8. 
;; All are available via pip. Some in emacs.
;; Elpy should be a repo in init.el. The rest will be via pip
;; Long term everything should be installed via emacs by mix of 
;; shell command calls and package.el

(elpy-enable)
(setq elpy-rpc-backend "jedi") ;;use jedi for AC instead of Rope default
(elpy-use-ipython)

;; (pyenv-activate)

