;; Change all questions from yes/no to y/n
(fset 'yes-or-no-p 'y-or-n-p)

;; No more ~ lock files
(setq create-lockfiles nil)

;; Jump to buffer on startup
(setq inhibit-startup-message t)
