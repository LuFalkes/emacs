;;;
;;; for OCaml. To install please follow Real World Setup Guide.
;;; https://github.com/realworldocaml/book/wiki/Installation-Instructions
;;; Also this might help
;;; https://github.com/OCamlPro/opam-user-setup
;;; Remember that camlp4 needs to be installed using the distro package manager
;;; If you get errors about camlp4 not being found during other package install,
;;; force a switch to the latest compiler with "opam switch $version-to-switch-to"

;; This should add all emacs packages in the .opam dir to the path for easy loading
(setq opam-share (substring (shell-command-to-string "opam config var share") 0 -1))
(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))

;; Pretty much stolen straight from the real world ocaml readme
(add-hook 'tuareg-mode-hook 'tuareg-imenu-set-imenu)
(setq auto-mode-alist
      (append '(("\\.ml[ily]?$" . tuareg-mode)
                ("\\.topml$" . tuareg-mode))
              auto-mode-alist)) 
(autoload 'utop-setup-ocaml-buffer "utop" "Toplevel for OCaml" t)
(add-hook 'tuareg-mode-hook 'utop-setup-ocaml-buffer)
(add-hook 'tuareg-mode-hook 'merlin-mode)
(setq merlin-use-auto-complete-mode t)
(setq merlin-error-after-save nil)

;; -- merlin setup ---------------------------------------

(setq opam-share (substring (shell-command-to-string "opam config var share 2> /dev/null") 0 -1))
(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))
(require 'merlin)

;; Enable Merlin for ML buffers
(add-hook 'tuareg-mode-hook 'merlin-mode)
(add-hook 'caml-mode-hook 'merlin-mode)
;; So you can do it on a mac, where `C-<up>` and `C-<down>` are used
;; by spaces.
(define-key merlin-mode-map
  (kbd "C-c <up>") 'merlin-type-enclosing-go-up)
(define-key merlin-mode-map
  (kbd "C-c <down>") 'merlin-type-enclosing-go-down)
(set-face-background 'merlin-type-face "#88FF44")

;; -- enable auto-complete -------------------------------
;; Not required, but useful along with merlin-mode. This is not company mode but the build in completion mode.
;; (require 'auto-complete)
;; (add-hook 'tuareg-mode-hook 'auto-complete-mode)

;; Make company aware of merlin
(with-eval-after-load 'company
  (add-to-list 'company-backends 'merlin-company-backend))
;; Enable company on merlin managed buffers
(add-hook 'merlin-mode-hook 'company-mode)

;; for ocp indent
;; the below 2 lines came up when i ran opam install ocp-indent
;; other 2 came from the real world ocaml guide.
;; (add-to-list 'load-path "/home/lu/.opam/4.02.3/share/emacs/site-lisp") ;;from opam suggestions
;; (setq opam-share (substring (shell-command-to-string "opam config var share") 0 -1)) ;; from real world ocaml
;; (load-file (concat opam-share "/typerex/ocp-indent/ocp-indent.el")) ;; real world ocaml
(require 'ocp-indent) ;; opam suggestion

;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/custom/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line

;; (load-file "/home/lu/.opam/4.02.3/share/typerex/ocp-indent/ocp-indent.el")
